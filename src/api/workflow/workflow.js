import request from '@/utils/request'
const url = '/workflow'

/**
 * 分页查询流程定义
 * @param params
 */
export function pageWfDeploy (params) {
  return request({
    url: `${url}/definition/list`,
    method: 'get',
    params: params
  })
}

/**
 * 导入流程文件
 * @param data
 * @param params
 */
export function importXml (data, params) {
  return request({
    url: `${url}/definition/importFile`,
    method: 'post',
    data: data,
    params: params
  })
}

/**
 * 查看流程图
 * @param deployId
 */
export function viewWfImage (deployId) {
  return request({
    url: `${url}/definition/readImage/${deployId}`,
    method: 'get'
  })
}

/**
 * 查看流程图
 * @param procDefKey
 */
export function viewWfImageByKey (procDefKey) {
  return request({
    url: `${url}/definition/readImage/procDefKey`,
    method: 'get',
    params: procDefKey
  })
}

/**
 * 读取xml数据
 * @param deployId
 * @returns {AxiosPromise}
 */
export function viewXmlData (deployId) {
  return request({
    url: `${url}/definition/readXml/${deployId}`,
    method: 'get'
  })
}

/**
 * 保存设计器的流程图
 * @param data
 */
export function saveXmlModel (data) {
  return request({
    url: `${url}/definition/deployXml`,
    method: 'post',
    data: data
  })
}

/**
 * 查询待办
 * @param params
 */
export function findTodo (params) {
  return request({
    url: `${url}/task/findTodo`,
    method: 'get',
    params: params
  })
}

/**
 * 查询已办
 * @param params
 */
export function findDone (params) {
  return request({
    url: `${url}/task/findDone`,
    method: 'get',
    params: params
  })
}

/**
 * 我发起的流程
 * @param params
 */
export function pageMyProcess (params) {
  return request({
    url: `${url}/task/myProcess`,
    method: 'get',
    params: params
  })
}

/**
 * 获取审批记录
 * @param proInstId
 * @returns {AxiosPromise}
 */
export function findRecords (proInstId) {
  return request({
    url: `${url}/task/findRecords/${proInstId}`,
    method: 'get'
  })
}

/**
 * 获取审批流程图
 * @param proInstId
 * @returns {AxiosPromise}
 */
export function getTaskImage (proInstId) {
  return request({
    url: `${url}/task/image/${proInstId}`,
    method: 'get'
  })
}

/**
 * 获取业务流程数据
 * @param bizId
 * @returns {AxiosPromise}
 */
export function getWorkflow (bizId) {
  return request({
    url: `${url}/task/getWorkflow/${bizId}`,
    method: 'get'
  })
}
