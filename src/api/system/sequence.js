import request from '@/utils/request'

const url = '/system'

/**
 * 列表查询序列号规则
 * @param params
 */
export function listSequence (params) {
  return request({
    url: `${url}/sequence/page`,
    method: 'get',
    params: params
  })
}

/**
 * 查询序列号规则详细
 * @param ruleId
 */
export function getSequence (ruleId) {
  return request({
    url: `${url}/sequence/${ruleId}`,
    method: 'get'
  })
}

/**
 * 新增序列号规则
 * @param data
 * @param params
 */
export function saveSequence (data) {
  return request({
    url: `${url}/sequence/save`,
    method: 'post',
    data: data
  })
}

/**
 * 修改序列号规则
 * @param data
 * @param params
 */
export function updateSequence (data) {
  return request({
    url: `${url}/sequence/update`,
    method: 'put',
    data: data
  })
}

/**
 * 删除序列号规则
 * @param data
 * @param params
 */
export function deleteSequence (ruleIds) {
  return request({
    url: `${url}/sequence/delete/${ruleIds}`,
    method: 'delete'
  })
}
