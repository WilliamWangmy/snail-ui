import request from '@/utils/request'
const url = '/system'

/**
 * 列表查询租户管理
 * @param params
 */
export function listTenant (params) {
  return request({
    url: `${url}/tenant/page`,
    method: 'get',
    params: params
  })
}

/**
 * 查询租户管理详细
 * @param ID
 */
export function getTenant (ID) {
  return request({
    url: `${url}/tenant/${ID}`,
    method: 'get'
  })
}

/**
 * 新增租户管理
 * @param data
 * @param params
 */
export function saveTenant (data) {
  return request({
    url: `${url}/tenant/save`,
    method: 'post',
    data: data
  })
}

/**
 * 修改租户管理
 * @param data
 * @param params
 */
export function updateTenant (data) {
  return request({
    url: `${url}/tenant/update`,
    method: 'put',
    data: data
  })
}

/**
 * 删除租户管理
 * @param data
 * @param params
 */
export function deleteTenant (ids) {
  return request({
    url: `${url}/tenant/delete/${ids}`,
    method: 'delete'
  })
}
/**
 * 修改租户状态
 * @param data
 * @param params
 */
export function updateStatus (data) {
  return request({
    url: `${url}/tenant/updateStatus`,
    method: 'put',
    data: data
  })
}
