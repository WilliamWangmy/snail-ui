import request from '@/utils/request'
const url = '/file'

/**
 * 查询已办
 * @param params
 */
export function getFile (params) {
  return request({
    url: `${url}/file/list`,
    method: 'get',
    params: params
  })
}
