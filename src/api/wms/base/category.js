import request from '@/utils/request'
const url = '/base'

/**
 * 列表查询产品分类
 * @param params
 */
export function treeCategory (params) {
  return request({
    url: `${url}/category/tree`,
    method: 'get',
    params: params
  })
}

/**
 * 查询产品分类详细
 * @param categoryId
 */
export function getCategory (categoryId) {
  return request({
    url: `${url}/category/${categoryId}`,
    method: 'get'
  })
}

/**
 * 新增产品分类
 * @param data
 * @param params
 */
export function saveCategory (data) {
  return request({
    url: `${url}/category/save`,
    method: 'post',
    data: data
  })
}

/**
 * 修改产品分类
 * @param data
 * @param params
 */
export function updateCategory (data) {
  return request({
    url: `${url}/category/update`,
    method: 'put',
    data: data
  })
}

/**
 * 删除产品分类
 * @param data
 * @param params
 */
export function deleteCategory (categoryIds) {
  return request({
    url: `${url}/category/delete/${categoryIds}`,
    method: 'delete'
  })
}
/**
 * 修改分类状态
 * @param data
 * @param params
 */
export function updateStatus (data) {
  return request({
    url: `${url}/category/updateStatus`,
    method: 'put',
    data: data
  })
}
