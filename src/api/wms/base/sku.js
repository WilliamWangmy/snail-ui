import request from '@/utils/request'
const url = '/base'

/**
 * 列表查询sku信息
 * @param params
 */
export function listSku (params) {
  return request({
    url: `${url}/sku/page`,
    method: 'get',
    params: params
  })
}

/**
 * 查询sku信息详细
 * @param skuId
 */
export function getSku (skuId) {
  return request({
    url: `${url}/sku/${skuId}`,
    method: 'get'
  })
}

/**
 * 新增sku信息
 * @param data
 * @param params
 */
export function saveSku (data) {
  return request({
    url: `${url}/sku/save`,
    method: 'post',
    data: data
  })
}

/**
 * 修改sku信息
 * @param data
 * @param params
 */
export function updateSku (data) {
  return request({
    url: `${url}/sku/update`,
    method: 'put',
    data: data
  })
}

/**
 * 删除sku信息
 * @param data
 * @param params
 */
export function deleteSku (skuIds) {
  return request({
    url: `${url}/sku/delete/${skuIds}`,
    method: 'delete'
  })
}
