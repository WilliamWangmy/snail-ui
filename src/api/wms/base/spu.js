import request from '@/utils/request'
const url = '/base'

/**
 * 列表查询spu信息
 * @param params
 */
export function pageSpu (params) {
  return request({
    url: `${url}/spu/page`,
    method: 'get',
    params: params
  })
}

/**
 * 列表查询spu信息
 * @param params
 */
export function listSpu (params) {
  return request({
    url: `${url}/spu/list`,
    method: 'get',
    params: params
  })
}

/**
 * 查询spu信息详细
 * @param spuId
 */
export function getSpu (spuId) {
  return request({
    url: `${url}/spu/${spuId}`,
    method: 'get'
  })
}

/**
 * 新增spu信息
 * @param data
 * @param params
 */
export function saveSpu (data) {
  return request({
    url: `${url}/spu/save`,
    method: 'post',
    data: data
  })
}

/**
 * 修改spu信息
 * @param data
 * @param params
 */
export function updateSpu (data) {
  return request({
    url: `${url}/spu/update`,
    method: 'put',
    data: data
  })
}

/**
 * 删除spu信息
 * @param data
 * @param params
 */
export function deleteSpu (spuIds) {
  return request({
    url: `${url}/spu/delete/${spuIds}`,
    method: 'delete'
  })
}

/**
 * 更新spu状态
 * @param data
 * @param params
 */
export function updateStatus (data) {
  return request({
    url: `${url}/spu/updateStatus`,
    method: 'put',
    data: data
  })
}
