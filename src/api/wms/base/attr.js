import request from '@/utils/request'
const url = '/base'

/**
 * 分页查询属性
 * @param params
 */
export function pageAttr (params) {
  return request({
    url: `${url}/attr/page`,
    method: 'get',
    params: params
  })
}

/**
 * 列表查询属性
 * @param params
 */
export function listAttr (params) {
  return request({
    url: `${url}/attr/list`,
    method: 'get',
    params: params
  })
}

/**
 * 查询属性详细
 * @param attrId
 */
export function getAttr (attrId) {
  return request({
    url: `${url}/attr/${attrId}`,
    method: 'get'
  })
}

/**
 * 新增属性
 * @param data
 * @param params
 */
export function saveAttr (data) {
  return request({
    url: `${url}/attr/save`,
    method: 'post',
    data: data
  })
}

/**
 * 修改属性
 * @param data
 * @param params
 */
export function updateAttr (data) {
  return request({
    url: `${url}/attr/update`,
    method: 'put',
    data: data
  })
}

/**
 * 删除属性
 * @param data
 * @param params
 */
export function deleteAttr (attrIds) {
  return request({
    url: `${url}/attr/delete/${attrIds}`,
    method: 'delete'
  })
}

/**
 * 修改状态
 * @param data
 * @param params
 */
export function updateStatus (data) {
  return request({
    url: `${url}/attr/updateStatus`,
    method: 'put',
    data: data
  })
}
