import request from '@/utils/request'
const url = '/oa'

/**
 * 列表查询请假申请
 * @param params
 */
export function listLeave (params) {
  return request({
    url: `${url}/leave/page`,
    method: 'get',
    params: params
  })
}

/**
 * 查询请假申请详细
 * @param applyId
 */
export function getLeave (applyId) {
  return request({
    url: `${url}/leave/${applyId}`,
    method: 'get'
  })
}

/**
 * 新增请假申请
 * @param data
 * @param params
 */
export function saveLeave (data) {
  return request({
    url: `${url}/leave/save`,
    method: 'post',
    data: data
  })
}

/**
 * 修改请假申请
 * @param data
 * @param params
 */
export function updateLeave (data) {
  return request({
    url: `${url}/leave/update`,
    method: 'put',
    data: data
  })
}

/**
 * 删除请假申请
 * @param data
 * @param params
 */
export function deleteLeave (applyIds) {
  return request({
    url: `${url}/leave/delete/${applyIds}`,
    method: 'delete'
  })
}

/**
 * 初始化流程
 * @param data
 * @returns {AxiosPromise}
 */
export function initWorkflow (data) {
  return request({
    url: `${url}/leave/initWorkflow`,
    method: 'post',
    data: data
  })
}

/**
 * 提交任务
 * @param data
 */
export function submit (data) {
  return request({
    url: `${url}/leave/submitTask`,
    method: 'post',
    data: data
  })
}
/**
 * 驳回任务
 * @param data
 */
export function reject (data) {
  return request({
    url: `${url}/leave/rejectTask`,
    method: 'post',
    data: data
  })
}
/**
 * 取消任务
 * @param data
 */
export function cancel (data) {
  return request({
    url: `${url}/leave/cancelTask`,
    method: 'post',
    data: data
  })
}

/**
 * 初始化下一任务节点
 * @param params
 * @returns {AxiosPromise}
 */
export function initNextNode (params) {
  return request({
    url: `${url}/leave/initNextNode`,
    method: 'get',
    params: params
  })
}
