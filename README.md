<h1 align="center">snail-ui</h1>

------------------------------

## 简介

**snail-ui**，使用[snail](https://gitee.com/WilliamWangmy/snail)作为后端，前端开发使用Ant Design Vue组件。

> snail-ui是基于SpringBoot，Spring Security，JWT，Vue 的前后端分离权限管理系统。
>
> 拥有用户管理、部门管理、岗位管理、菜单管理、角色管理、字典管理、参数管理、通知公告、操作日志、登录日志、在线用户、定时任务、代码生成、系统接口、服务监控、流程设计、流程管理、序列号规则管理等功能。



## 开始使用

1. 环境准备
   * 安装[node](http://nodejs.org/)和[git](https://git-scm.com/)

2. 安装

   ```shell
   git clone https://gitee.com/WilliamWangmy/snail-ui.git
   ```

   或

   ```shell
   git clone git@gitee.com:WilliamWangmy/snail-ui.git
   ```

3. 本地开发

   进入项目根目录

   ```shell
   npm install
   ```

   > 若耗时太长可使用`npm install --registry=https://registry.npm.taobao.org`

   ```shell
   npm run serve
   ```

   > 打开浏览器访问 [http://localhost:8000](http://localhost:8080/)

4. 部署

   

   

## 注意事项





## 致谢

* [RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue) 基于SpringBoot，Spring Security，JWT，Vue 的前后端分离权限管理系统

* [Ant Design Vue](https://github.com/vueComponent/ant-design-vue/) An enterprise-class UI components based on Ant Design and Vue

* [Ant Design Vue Pro](https://github.com/vueComponent/ant-design-vue-pro) Use Ant Design Vue like a Pro

  

## 联系

如果您发现了什么bug，或者有什么界面建议或意见，

欢迎 [issue](https://gitee.com/WilliamWangmy/snail-ui/issues)

## 演示图

<table>
    <tr>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E7%99%BB%E5%BD%95%E9%A1%B5%E9%9D%A2.png"/></td>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E5%BE%85%E5%8A%9E%E3%80%81%E5%B7%B2%E5%8A%9E%E3%80%81%E5%BE%85%E9%98%85%E9%A1%B5%E9%9D%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86%E9%A1%B5%E9%9D%A2.png"/></td>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E9%83%A8%E9%97%A8%E7%AE%A1%E7%90%86%E9%A1%B5%E9%9D%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E8%A7%92%E8%89%B2%E7%AE%A1%E7%90%86%E9%A1%B5%E9%9D%A2.png"/></td>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E8%8F%9C%E5%8D%95%E7%AE%A1%E7%90%86%E9%A1%B5%E9%9D%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E6%B5%81%E7%A8%8B%E8%AE%BE%E8%AE%A1%E9%A1%B5%E9%9D%A2.png"/></td>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E6%B5%81%E7%A8%8B%E5%8F%91%E5%B8%83%E9%A1%B5%E9%9D%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E8%AF%A6%E6%83%85%E9%A1%B5%E9%9D%A2.png"/></td>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E6%8F%90%E4%BA%A4%E9%A1%B5%E9%9D%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90%E9%85%8D%E7%BD%AE%E6%95%B0%E6%8D%AE%E6%BA%90.png"/></td>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90%E9%85%8D%E7%BD%AE%E5%B1%9E%E6%80%A7.png"/></td>
    </tr>
</table>






